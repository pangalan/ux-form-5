package ux;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PrototypeV1Controller {
    public TableView<Product> productTable;
    public TableColumn<Product, String> titleColumn;
    public TableColumn<Product, String> codeColumn;
    public TableColumn<Product, String> unitColumn;
    public TableColumn<Product, String> amountColumn;
    public TableColumn<Product, String> costColumn;

    private List<Product> products = new ArrayList<>();

    @FXML
    private void initialize() {
        products.add(new Product("", "", UnitType.KG, 0.0, 0.0));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        codeColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unit"));
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        Set<String> units = Arrays.stream(UnitType.values())
                                  .map(Enum::name)
                                  .collect(Collectors.toSet());
        unitColumn.setCellFactory(ComboBoxTableCell.forTableColumn(FXCollections.observableArrayList(units)));
        productTable.setItems(FXCollections.observableArrayList(products));
    }

    public class Product {
        private String title;
        private String code;
        private UnitType unit;
        private Double amount;
        private Double cost;

        public Product() {
        }

        Product(String title, String code, UnitType unit, Double amount, Double cost) {
            this.title = title;
            this.code = code;
            this.unit = unit;
            this.amount = amount;
            this.cost = cost;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public UnitType getUnit() {
            return unit;
        }

        public void setUnit(UnitType unit) {
            this.unit = unit;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getCost() {
            return cost;
        }

        public void setCost(Double cost) {
            this.cost = cost;
        }
    }

    private enum UnitType {
        KG, G, T
    }


}
